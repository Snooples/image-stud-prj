<?php
return [
    'user.passwordResetTokenExpire' => 3600,
    'storagePath' => '@frontend/web/uploads/',
];
