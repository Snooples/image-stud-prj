<?php

namespace common\components;

use yii\web\UploadedFile;

/**
 *
 * @author Snooples
 */
interface StorageInterface
{
    public function saveUploadedFile(UploadedFile $file);

    public function getFile(string $filename);
}
