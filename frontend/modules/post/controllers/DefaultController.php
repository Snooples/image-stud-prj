<?php

namespace frontend\modules\post\controllers;

use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;
use frontend\models\Post;
use frontend\models\User;
use frontend\modules\post\models\forms\PostForm;
use yii\web\Response;
use frontend\modules\post\models\Comments;
use frontend\modules\post\models\forms\CommentForm;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `post` module
 */
class DefaultController extends Controller
{
    
    /**
     * Renders the create view for the module
     * @return string
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        $model = new PostForm(Yii::$app->user->identity);

        if ($model->load(Yii::$app->request->post())) {

            $model->picture = UploadedFile::getInstance($model, 'picture');

            if ($model->save()) {

                Yii::$app->session->setFlash('success', 'Post created');
                return $this->refresh();
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Renders the create view for the module
     * @param string $id
     * @return string
     */
    public function actionView($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }
        
        $model = new CommentForm();
        $comment = new Comments();

        $currentUrl = Url::remember();
             
        if ($model->load(Yii::$app->request->post())) {
            $model->postId = $id;
            $currentUser = Yii::$app->user->identity;
            if ($model->save()) {
                Comments::comment($currentUser, $id);
                return $this->refresh();
            }
        }
        
        /* @var $currentUser User */
        $currentUser = Yii::$app->user->identity;
        
        $comments = $comment->getComments($id);
        
        $post = $this->findPost($id);

        return $this->render('view', [
                    'post' => $post,
                    'currentUser' => $currentUser,
                    'model' => $model,
                    'comments' => $comments,
        ]);
    }

    public function actionLike()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');
        $post = $this->findPost($id);

        /* @var $currentUser User */
        $currentUser = Yii::$app->user->identity;

        $post->like($currentUser);

        return [
            'success' => true,
            'likesCount' => $post->countLikes(),
        ];
    }

    public function actionUnlike()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');

        /* @var $currentUser User */
        $currentUser = Yii::$app->user->identity;
        $post = $this->findPost($id);

        $post->unLike($currentUser);

        return [
            'success' => true,
            'likesCount' => $post->countLikes(),
        ];
    }

    public function actionDeleteComment($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        $currentUrl = Url::previous();

        $comment = Comments::findOne($id);
        $post = $comment->post->getId();

        if ($comment && $comment->delete()) {
            Comments::unComment($post);
            return $this->redirect($currentUrl);
        }
    }

    public function actionEditComment($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        
        $comment = Comments::findOne($id);

        if ($comment->load(Yii::$app->request->post())) {
            if ($comment->save()) {
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        return $this->render('editComment', [
                    'comment' => $comment,
                ]);
    }

    
    public function actionEdit($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }
        
        $post = Post::findOne($id);
        
        $currentUrl = Url::previous();

        if ($post->load(Yii::$app->request->post())) {
            if ($post->trueSave($post)) {
                return $this->redirect($currentUrl);
            }
        }
        return $this->render('edit', [
                    'post' => $post,
                ]);
           
    }

    /**
     * @param integer $id
     * @return User
     * @throws NotFoundException
     */
    private function findPost($id)
    {
        if ($user = Post::findOne($id)) {
            return $user;
        }
        throw new NotFoundHttpException('Post not found');
    }
    
    public function actionComplain()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }
        
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $id = Yii::$app->request->post('id');
        
        /* @var $currentUser User */
        $currentUser = Yii::$app->user->identity;
        $post = $this->findPost($id);
        
        if ($post->complain($currentUser)) {
            return [
                'success' => true,
                'text' => 'Post reported'
            ];
        }
        return [
                'success' => false,
                'text' => 'Error'
            ];
    }

}
