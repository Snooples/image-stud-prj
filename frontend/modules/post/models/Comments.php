<?php

namespace frontend\modules\post\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;
use frontend\models\User;
use frontend\models\Post;

/**
 * This is the model class for table "comments".
 *
 * @property int $id
 * @property int $author_id
 * @property int $post_id
 * @property string $content
 * @property int $created_at
 * @property int $updated_at
 */
class Comments extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comments';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'author_id',
                'updatedByAttribute' => false,
            ],
        ];
    }
    
    public function rules()
    {
        return [
            [['content'], 'string', 'max' => 1000],
        ];
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author_id' => 'Author ID',
            'post_id' => 'Post ID',
            'content' => 'Content',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get author of the post
     * @return User|null
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }
    
    public function getPostId()
    {
        return $this->post_id;
    }
    
    public function getAuthorId()
    {
        return $this->author_id;
    }
    
    public function getComments($id)
    {
        return Comments::find()->where(['post_id' => $id])->indexBy('id')->all();
    }
    
    /**
     * Comment current post by given user
     * @param \frontend\models\User $user
     */
    public static function comment(User $user, $id)
    {
        /* @var redis Connection */
        $redis = Yii::$app->redis;
        if ($redis->get("post:{$id}:comments") > 0) {
            $redis->incr("post:{$id}:comments");
            return true;
        } else {
            $redis->set("post:{$id}:comments", 1);
            return true;
        }
        return false;
        
    }
    
    /**
     * Delete comment current post by given user
     * @param \frontend\models\User $user
     */
    public static function unComment($id)
    {
        /* @var redis Connection */
        $redis = Yii::$app->redis;
        if ($redis->get("post:{$id}:comments") > 0) {
            $redis->decr("post:{$id}:comments");  
        }

    }
}
