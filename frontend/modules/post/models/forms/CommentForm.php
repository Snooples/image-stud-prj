<?php

namespace frontend\modules\post\models\forms;

use Yii;
use yii\base\Model;
use frontend\models\Post;
use frontend\models\User;
use frontend\modules\post\models\Comments;

class CommentForm extends Model
{

    const MAX_DESCRIPTION_LENGHT = 1000;
    
    public $content;
    public $postId;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string', 'max' => self::MAX_DESCRIPTION_LENGHT],
            [['postId'], 'integer'],
        ];
    }
    
    /**
     * @return boolean
     */
    public function save()
    {
        if ($this->validate()) {
            
            $comment = new Comments();
            
            $comment->content = $this->content;
            $comment->post_id = $this->postId;
            return $comment->save(false);
        }
    }
    
}
