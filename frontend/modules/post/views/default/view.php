<?php
/* @var $this yii\web\View */
/* @var $model frontend\modules\models\forms\CommentForm */
/* @var $comment frontend\modules\models\Comments */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\HtmlPurifier;
use yii\web\JqueryAsset;

$this->title = ('Post №' . $post->id);
?>


<div class="page-posts no-padding">
    <div class="row">
        <div class="page page-post col-sm-12 col-xs-12 post-82">


            <div class="blog-posts blog-posts-large">

                <div class="row">

                    <!-- feed item -->
                    <article class="post col-sm-12 col-xs-12">                                            
                        <div class="post-meta">
                            <div class="post-title">
                                <img src="<?php echo $post->user->getPicture(); ?>" class="author-image" />
                                <div class="author-name"><a href="<?php echo Url::to(['/user/profile/view/', 'nickname' => ($post->user->nickname) ? $post->user->nickname : $post->user->id]); ?>"><?php echo Html::encode($post->user->username); ?></a></div>
                            </div>
                        </div>
                        <div class="post-type-image">
                            <a>
                                <img src="<?php echo $post->getImage(); ?>" alt="">
                            </a>
                        </div>
                        <br>
                        <div class="post-description">
                            <p><?php echo HTMLPurifier::process($post->description); ?></p>
                        </div>
                        <hr>
                        <div class="post-bottom">
                            <div class="post-likes">
                                <i class="fa fa-lg fa-heart-o"></i>
                                <span class="likes-count"><?php echo Html::encode($post->countLikes()); ?></span>
                                &nbsp;&nbsp;&nbsp;
                                <a href="#" class="btn btn-default button-unlike <?php echo ($currentUser && $post->isLikedBy($currentUser)) ? "" : "display-none"; ?>" data-id="<?php echo $post->id; ?>">
                                    Unlike&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-down"></span>
                                </a>
                                <a href="#" class="btn btn-default button-like <?php echo ($currentUser && $post->isLikedBy($currentUser)) ? "display-none" : ""; ?>" data-id="<?php echo $post->id; ?>">
                                    Like&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-up"></span>
                                </a>
                            </div>
                            <div class="post-comments">
                                <a href="#">
                                    <?php echo Html::encode($post->countComments()); ?> Comments
                                </a>

                            </div>
                            <div class="post-date">
                                <span><?php echo Yii::$app->formatter->asDate($post->created_at); ?></span>    
                            </div>
                            <div class="post-report">
                                        <?php if (!$post->isReported($currentUser)): ?>
                                        <a href="#" class="btn btn-default button-complain" data-id="<?php echo $post->id; ?>">
                                            Report post <i class="fa fa-cog fa-spin fa-fw icon-preloader" style="display:none"></i>
                                        </a>
                                        <?php else: ?>
                                        <p>Post has been reported</p>
                                        <?php endif; ?>
                                    </div>
                            <div class="post-report">
                                <a href="<?php echo Url::to(['/post/default/edit', 'id' => $post->id]); ?>" class="btn btn--outline-secondary" role="button">Edit</a>
                            </div>
                        </div>
                    </article>
                    <!-- feed item -->


                    <div class="col-sm-12 col-xs-12">
                        <?php echo Html::encode($post->countComments()); ?> Comments

                        <!-- comments-post -->
                        <div class="comments-post">

                            <div class="single-item-title"></div>
                            <div class="row">
                                <ul class="comment-list">

                                    <!-- comment item -->
                                    <li class="comment">
                                        <?php foreach ($comments as $item): ?>
                                            <div class="comment-user-image">
                                                <img src="<?php echo Yii::$app->storage->getFile($item->user->picture); ?>">
                                            </div>
                                            <div class="comment-info">
                                                <h4 class="author"><a href="#"><?php echo Html::encode($item->user->username); ?></a> <span class>(<?php echo Yii::$app->formatter->asDate($item->created_at); ?>)</span></h4>
                                                <p><?php echo Html::encode($item->content); ?></p>
                                                <?php if ($post->user && $currentUser->equals($post->user)): ?>
                                                    <a href="<?php echo Url::to(['/post/default/delete-comment', 'id' => $item->id]); ?>" class="btn btn-outline-secondary btn-sm" role="button">Delete</a>
                                                <?php endif; ?>
                                                <?php if ($item->user && $currentUser->equals($item->user)): ?>
                                                    <a href="<?php echo Url::to(['/post/default/edit-comment', 'id' => $item->id]); ?>" class="btn btn-outline-secondary btn-sm" role="button">Edit</a>
                                                <?php endif; ?>
                                            </div>

                                        </li>
                                    <?php endforeach; ?>
                                    <!-- comment item -->

                                </ul>
                            </div>

                        </div>
                        <!-- comments-post -->
                    </div>

                    <div class="col-sm-12 col-xs-12">
                        <div class="comment-respond">
                            <?php $form = ActiveForm::begin(); ?>

                            <?php echo $form->field($model, 'content')->textInput(['autofocus' => true])->label('<h4>Leave a Reply</h4>'); ?>

                            <?php echo Html::submitButton('Send', ['class' => 'btn btn-secondary', 'href' => Url::to(['/post/default/comment'])]); ?>

                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






</div>



<?php
$this->registerJsFile('@web/js/likes.js', [
    'depends' => JqueryAsset::className(),
]);
$this->registerJsFile('@web/js/complaints.js', [
    'depends' => JqueryAsset::className(),
]);
