<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="post-default-index">

    <h1>Create post</h1>

    <?php $form = ActiveForm::begin(); ?>

        <?php echo $form->field($comment, 'content'); ?>

        <?php echo Html::submitButton('Edit', ['class' => 'btn btn-primary']); ?>

    <?php ActiveForm::end(); ?>

</div>