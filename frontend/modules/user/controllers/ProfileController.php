<?php

namespace frontend\modules\user\controllers;

use Yii;
use yii\web\Controller;
use frontend\models\User;
use yii\web\NotFoundHttpException;
use frontend\modules\user\models\forms\PictureForm;
use yii\web\UploadedFile;
use yii\web\Response;
use yii\helpers\Url;
use frontend\models\Post;

/**
 * Default controller for the `user` module
 */
class ProfileController extends Controller
{
        
    /**
     * Display profile page
     * 
     * @return mixed
     */
    public function actionView($nickname)
    {

        $currentUser = Yii::$app->user->identity;

        $modelPicture = new PictureForm();

        $user = $this->findUser($nickname);

        $posts = $user->getPost();

        return $this->render('view', [
                    'currentUser' => $currentUser,
                    'user' => $user,
                    'modelPicture' => $modelPicture,
                    'posts' => $posts,
        ]);
    }

    public function actionUploadPicture()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new PictureForm();
        $model->picture = UploadedFile::getInstance($model, 'picture');

        if ($model->validate()) {

            $user = Yii::$app->user->identity;
            $user->picture = Yii::$app->storage->saveUploadedFile($model->picture);

            if ($user->trueSave($user)) {
                return [
                    'success' => true,
                    'pictureUri' => Yii::$app->storage->getFile($user->picture),
                ];
            }
        }
        return ['success' => false, 'errors' => $model->getErrors()];
    }

    public function actionDeletePicture()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        /* @var $currentUser User */
        $currentUser = Yii::$app->user->identity;

        if ($currentUser->deletePicture()) {
            Yii::$app->session->setFlash('success', 'Picture deleted');
        } else {
            Yii::$app->session->setFlash('danger', 'Error occured');
        }
        $currentUser->trueSave($currentUser);
        
        return $this->redirect(['/user/profile/view', 'nickname' => $currentUser->getNickname()]);
    }

    /**
     * @param string $nickname
     * @return type
     * @throws NotFoundHttpException
     */
    public function findUser($nickname)
    {
        if ($user = User::find()->where(['nickname' => $nickname])->orWhere(['id' => $nickname])->one()) {
            return $user;
        }
        throw new NotFoundHttpException('User not found');
    }

    /**
     * tech of subscribe
     * @param mixed $id
     * @return bool
     */
    public function actionSubscribe($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        /* @var $currentUser User */
        $currentUser = Yii::$app->user->identity;

        $user = $this->findUserById($id);

        $currentUser->followUser($user);

        return $this->redirect(['/user/profile/view', 'nickname' => $user->getNickName()]);
    }

    /**
     * 
     * @param integer $id
     * @return User
     * @throws NotFoundHttpException
     */
    public function findUserById($id)
    {
        if ($user = User::findOne($id)) {
            return $user;
        }
        throw new NotFoundHttpException;
    }

    /**
     * tech of unsubscribe
     * @param mixed $id
     * @return bool
     */
    public function actionUnsubscribe($id)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        /* @var $currentUser User */
        $currentUser = Yii::$app->user->identity;

        $user = $this->findUserById($id);

        $currentUser->unfollowUser($user);

        return $this->redirect(['/user/profile/view', 'nickname' => $user->getNickName()]);
    }
    
    public function actionEdit($id)
    {
        $user = User::findOne($id);
        
        if ($user->load(Yii::$app->request->post())) {
            if ($user->trueSave($user)) {
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        return $this->render('edit', [
                    'user' => $user,
                ]);
           
    }

//    public function actionGenerate()
//    {
//        ini_set('max_execution_time', 60);
//        $faker = \Faker\Factory::create();
//        $fakerAbout = \Faker\Factory::create('Ru_RU');
//        for ($i = 0; $i < 1000; $i++) {
//            $user = new User([
//                'username' => $faker->name,
//                'email' => $faker->freeEmail,
//                'nickname' => $faker->regexify('[A-Za-z0-9_]{5,15}'),
//                'about' => $fakerAbout->realText(200),
//                'auth_key' => Yii::$app->security->generateRandomString(),
//                'password_hash' => Yii::$app->security->generatePasswordHash($faker->password(6,12)),
//                'created_at' => $time = $faker->unixTime(),
//                'updated_at' => $time,
//            ]);
//            $user->save(false);
//        }
//    }
}
