<?php

namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;
use Intervention\Image\ImageManager;

/**
 * Description of PictureForm
 *
 * @author Snooples
 */
class PictureForm extends Model
{

    public $picture;

    public function __construct()
    {
        $this->on(self::EVENT_AFTER_VALIDATE, [$this, 'resizePicture']);
    }

    public function rules()
    {
        return [
            [['picture'], 'file',
                'extensions' => ['jpg'],
                'checkExtensionByMimeType' => true,
                'maxSize' => Yii::$app->params['maxFileSize'],
            ],
        ];
    }

    public function save()
    {
        return 1;
    }

    /**
     * Resize image if needed
     */
    public function resizePicture()
    {
        if ($this->picture->error) {
            /* В объекте UploadedFile есть свойство error. Если в нем "1", значит
             * произошла ошибка и работать с изображением не нужно, прерываем
             * выполнение метода */
            return;
        }

        $width = 250;
        $height = 250;

        $manager = new ImageManager(array('driver' => 'imagick'));

        $image = $manager->make($this->picture->tempName);

        // 3-й аргумент - органичения - специальные настройки при изменении размера
        $image->resize($width, $height, function ($constraint) {

            // Пропорции изображений оставлять такими же (например, для избежания широких или вытянутых лиц)
            $constraint->aspectRatio();
        })->save();
    }

}
