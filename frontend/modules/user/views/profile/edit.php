<?php
/* @var $user frontend\models\Post */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="user-default-index">

    <h1>Edit profile</h1>

    <?php $form = ActiveForm::begin(); ?>

    <a href="<?php echo Url::to(['/user/profile/delete-picture']); ?>" class="btn btn-danger" id="profile-delete-picture">Delete picture</a>

    <br><br>

    <?php echo $form->field($user, 'nickname'); ?>
    
    <?php echo $form->field($user, 'about'); ?>

    <?php echo Html::submitButton('Save', ['class' => 'btn btn-primary']); ?>

    <?php ActiveForm::end(); ?>

</div>