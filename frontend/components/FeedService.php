<?php

namespace frontend\components;

use Yii;
use yii\base\Component;
use frontend\models\Feed;

/**
 * Feed component
 * 
 * @author Snooples
 */
class FeedService extends Component
{
    
    
    public function addToFeeds(\yii\base\Event $event)
    {
        
        $user = $event->getUser();
        $post = $event->getPost();
        
        $followers = $user->getFollowers();
        
        foreach ($followers as $follower) {
            $feedItem = new Feed();
            $feedItem->user_id = $follower['id'];
            $feedItem->author_id = $user->id;
            $feedItem->author_name = $user->username;
            $feedItem->author_nickname = $user->getNickname();
            $feedItem->author_picture = $user->getPicture();
            $feedItem->post_id = $post->id;
            $feedItem->post_filename = $post->filename;
            $feedItem->post_description = $post->description;
            $feedItem->post_created_at = $post->created_at;
            $feedItem->save();
        }
    }
    
    public function editPostInfo(\yii\base\Event $event)
    {
        $post = $event->getPost();
        if ($post) {
            
            $feedItem = Feed::find()->where(['post_id' => $post->id])->all();

            foreach ($feedItem as $item) {
                $item->post_description = $post->description;
                $item->save();
            } 
        }
    }
    
    public function editUserInfo(\yii\base\Event $event)
    {
        $user = $event->getUser();
        if ($user) {
            
            $feedItem = Feed::find()->where(['author_id' => $user->id])->all();
            
            foreach ($feedItem as $item) {
                
                $item->author_nickname = $user->nickname;
                $item->author_picture = $user->getPicture();
                $item->save();
            };
        }
    }
    
    
    
}
