<?php

return [
    'Signup' => 'Регистрация',
    'Login' => 'Вход',
    'My Profile' => 'Мой профиль',
    'Create post' => 'Добавить пост',
    'Newsfeed' => 'Лента новостей',
    'Logout ({username})' => 'Выход ({username})',
    '' => '',
];