<?php
return [
    'adminEmail' => 'admin@example.com',
    
    'maxFileSize' => 1024 * 1024 * 4,
    
    'storageUri' => '/uploads/',
    
    'postPicture' => [
        'maxWidth' => 1024,
        'maxHeight' => 768,
    ],
    
    'feedPostLimit' => 200,
    
    'supportedLanguages' => [
        'ru-RU' => 'Русский',
        'en-US' => 'English',
    ],
];
