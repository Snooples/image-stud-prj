<?php

namespace backend\models;

use Yii;
use backend\models\Feed;

/**
 * This is the model class for table "post".
 *
 * @property int $id
 * @property int $user_id
 * @property string $filename
 * @property string $description
 * @property int $created_at
 * @property int $edited_at
 * @property int $complaints
 */
class Post extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'filename' => 'Filename',
            'description' => 'Description',
            'created_at' => 'Created At',
            'edited_at' => 'Edited At',
            'complaints' => 'Complaints',
        ];
    }

    public static function findComplaints()
    {
        return Post::find()->where('complaints > 0')->orderBy('complaints DESC');
    }

    public function getImage()
    {
        return Yii::$app->storage->getFile($this->filename);
    }

    public function approve()
    {
        /* @var $redis Connection */
        $redis = Yii::$app->redis;
        $key = "post:{$this->id}:complaints";
        if ($redis->del($key)) {
            $this->complaints = 0;
            return $this->save(false, ['complaints']);
        }
    }
    
    /**
     * @param integer $limit
     * @return array
     */
    public function getFeed()
    {
        return $this->hasOne(Feed::className(), ['post_id' => 'id']);
    }
    
    public function deleteFeedsByPost($id)
    {
        $feeds = Feed::find()->where(['post_id' => $id])->all();
        foreach ($feeds as $feed) {
            $feed->delete();
        }
    }
    
    public function deleteLikesByPost($id)
    {
        /* @var $redis Connection */
        $redis = Yii::$app->redis;
        $key = "post:{$this->id}:likes";
        if ($redis->scard($key) > 0){
            return $redis->del("post:{$id}:likes");
        }
    }
    
    public function deleteComplaintsByPost($id)
    {
        /* @var $redis Connection */
        $redis = Yii::$app->redis;
        $key = "post:{$this->id}:complaints";
        if ($redis->scard($key) > 0){
            return $redis->del("post:{$id}:complaints");
        }
    }

}
